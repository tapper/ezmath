import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import {LoadingController} from "ionic-angular";
import { AppSettings } from './app-settings'
import {Storage} from '@ionic/storage';
import 'rxjs/Rx';

import {Http,Headers,Response, RequestOptions} from "@angular/http";
import { HttpModule } from '@angular/http';


@Injectable()
export class ServerService {

    constructor(public loadingCtrl: LoadingController,
                public storage: Storage,
                private http: Http) {
    }


    GetBranches(url) {
        let loading = this.loadingCtrl.create({content: 'Please wait...'});
        loading.present();
        try {
            let body = new FormData();
            return this.http.post(AppSettings.SERVER_URL + '' + url, body).map(res => res).do((data) => {
            }).toPromise();
        } catch (err) {
            console.log( err);
        } finally {
            loading.dismiss();
        }
    }

    GetProfessions(url) {
        let loading = this.loadingCtrl.create({content: 'Please wait...'});
        loading.present();
        try {
            let body = new FormData();
            return this.http.post(AppSettings.SERVER_URL + '' + url, body).map(res => res).do((data) => {
            }).toPromise();
        } catch (err) {
            console.log( err);
        } finally {
            loading.dismiss();
        }
    }

    SearchClasses(url,data,user_id) {
        let loading = this.loadingCtrl.create({content: 'Please wait...'});
        loading.present();
        try {
            let body = new FormData();
            body.append("category", JSON.stringify(data));
            body.append("user_id", user_id);
            return this.http.post(AppSettings.SERVER_URL + '' + url, body).map(res => res).do((data) => {
            }).toPromise();
        } catch (err) {
            console.log( err);
        } finally {
            loading.dismiss();
        }
    }

    ClassPayment(url,data,user_id) {
        let loading = this.loadingCtrl.create({content: 'Please wait...'});
        loading.present();
        try {
            let body = new FormData();
            body.append("category", JSON.stringify(data));
            body.append("user_id", user_id);
            return this.http.post(AppSettings.SERVER_URL + '' + url, body).map(res => res).do((data) => {
            }).toPromise();
        } catch (err) {
            console.log( err);
        } finally {
            loading.dismiss();
        }
    }

    userDidntShow(url,data,user_id) {
        let loading = this.loadingCtrl.create({content: 'Please wait...'});
        loading.present();
        try {
            let body = new FormData();
            body.append("category", JSON.stringify(data));
            body.append("user_id", user_id);
            return this.http.post(AppSettings.SERVER_URL + '' + url, body).map(res => res).do((data) => {
            }).toPromise();
        } catch (err) {
            console.log( err);
        } finally {
            loading.dismiss();
        }
    }

    registerUserToClass(url,user_id,class_id) {
        let loading = this.loadingCtrl.create({content: 'Please wait...'});
        loading.present();
        try {
            let body = new FormData();
            body.append("user_id", user_id);
            body.append("class_id", JSON.stringify(class_id));
            return this.http.post(AppSettings.SERVER_URL + '' + url, body).map(res => res).do((data) => {
            }).toPromise();
        } catch (err) {
            console.log( err);
        } finally {
            loading.dismiss();
        }
    }

    getStudentData(url,user_id) {
        let loading = this.loadingCtrl.create({content: 'Please wait...'});
        loading.present();
        try {
            let body = new FormData();
            body.append("user_id", user_id);
            return this.http.post(AppSettings.SERVER_URL + '' + url, body).map(res => res).do((data) => {
            }).toPromise();
        } catch (err) {
            console.log( err);
        } finally {
            loading.dismiss();
        }
    }

    saveUserProfile(url,data,user_id) {
        let loading = this.loadingCtrl.create({content: 'Please wait...'});
        loading.present();
        try {
            let body = new FormData();
            body.append("category", JSON.stringify(data));
            body.append("user_id", user_id);
            return this.http.post(AppSettings.SERVER_URL + '' + url, body).map(res => res).do((data) => {
            }).toPromise();
        } catch (err) {
            console.log( err);
        } finally {
            loading.dismiss();
        }
    }


    getFutureClasses(url,user_id) {
        let loading = this.loadingCtrl.create({content: 'Please wait...'});
        loading.present();
        try {
            let body = new FormData();
            body.append("user_id", user_id);
            return this.http.post(AppSettings.SERVER_URL + '' + url, body).map(res => res).do((data) => {
            }).toPromise();
        } catch (err) {
            console.log( err);
        } finally {
            loading.dismiss();
        }
    }

    cancelFutureClass(url,class_id,user_id) {
        let loading = this.loadingCtrl.create({content: 'Please wait...'});
        loading.present();
        try {
            let body = new FormData();
            body.append("user_id", user_id);
            body.append("class_id", class_id);
            return this.http.post(AppSettings.SERVER_URL + '' + url, body).map(res => res).do((data) => {
            }).toPromise();
        } catch (err) {
            console.log( err);
        } finally {
            loading.dismiss();
        }
    }

    getPastClasses(url,user_id) {
        let loading = this.loadingCtrl.create({content: 'Please wait...'});
        loading.present();
        try {
            let body = new FormData();
            body.append("user_id", user_id);
            return this.http.post(AppSettings.SERVER_URL + '' + url, body).map(res => res).do((data) => {
            }).toPromise();
        } catch (err) {
            console.log( err);
        } finally {
            loading.dismiss();
        }
    }

    GetHomePageData(url,user_id) {
        //let loading = this.loadingCtrl.create({content: 'Please wait...'});
        //loading.present();
        try {
            let body = new FormData();
            body.append("user_id", user_id);
            return this.http.post(AppSettings.SERVER_URL + '' + url, body).map(res => res).do((data) => {
            }).toPromise();
        } catch (err) {
            console.log( err);
        } finally {
            //loading.dismiss();
        }
    }

    GetpreviousTickets(url,user_id) {
        let loading = this.loadingCtrl.create({content: 'Please wait...'});
        loading.present();
        try {
            let body = new FormData();
            body.append("user_id", user_id);
            return this.http.post(AppSettings.SERVER_URL + '' + url, body).map(res => res).do((data) => {
            }).toPromise();
        } catch (err) {
            console.log( err);
        } finally {
            loading.dismiss();
        }
    }


    ContactSignup(url,data,user_id) {
        let loading = this.loadingCtrl.create({content: 'Please wait...'});
        loading.present();
        try {
            let body = new FormData();
            body.append("category", JSON.stringify(data));
            body.append("user_id", user_id);
            return this.http.post(AppSettings.SERVER_URL + '' + url, body).map(res => res).do((data) => {
            }).toPromise();
        } catch (err) {
            console.log( err);
        } finally {
            loading.dismiss();
        }
    }

    GetData(url) {
        //let loading = this.loadingCtrl.create({content: 'Please wait...'});
        //loading.present();
        try {
            let body = new FormData();
            return this.http.post(AppSettings.SERVER_URL + '' + url, body).map(res => res).do((data) => {
            }).toPromise();
        } catch (err) {
            console.log( err);
        } finally {
           // loading.dismiss();
        }
    }


    getPushNotifications(url,user_id) {
        let loading = this.loadingCtrl.create({content: 'Please wait...'});
        loading.present();
        try {
            let body = new FormData();
            body.append("user_id", user_id);
            return this.http.post(AppSettings.SERVER_URL + '' + url, body).map(res => res).do((data) => {
            }).toPromise();
        } catch (err) {
            console.log( err);
        } finally {
            loading.dismiss();
        }
    }
}




//this._categories.next(categories);
//resolve(this.categories);