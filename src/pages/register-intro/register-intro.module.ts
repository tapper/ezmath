import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RegisterIntroPage } from './register-intro';
import {ComponentsModule} from '../../components/components.module';

@NgModule({
  declarations: [
    RegisterIntroPage,
  ],
  imports: [
    IonicPageModule.forChild(RegisterIntroPage),ComponentsModule,

  ],
})
export class RegisterIntroPageModule {}
