import { Component, ViewChild } from '@angular/core';
import {Nav, Platform, ToastController} from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AppSettings } from '../services/app-settings'
import {Firebase} from "@ionic-native/firebase";
import {AuthService} from "../services/auth-service";
import {Network} from "@ionic-native/network";
import {ToastService} from "../services/toast-service";
import { Observable } from 'rxjs/Observable';
import {PushnotificationsPage} from "../pages/pushnotifications/pushnotifications";

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
    previousStatus:any


    pages: Array<{title: string, component: any}>;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen, private firebase: Firebase,
              public auth:AuthService,private toastCtrl: ToastController) {
    this.initializeApp();

      /*
      this.network.onDisconnect().subscribe( () => {
          if (this.previousStatus == "Online") {
              this.Toast.presentToast('יש לבדוק חיבור לאינטרנט');
          }
          this.previousStatus = "Offline";
      });
      Network.onConnect().subscribe(() => {
          if (this.previousStatus == "Offline") {
          }
          this.previousStatus = "Online";
      });
      */

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Home', component: 'HomePage' }
    ];

  }

  async initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();



        var offline = Observable.fromEvent(window, "offline");
        var online = Observable.fromEvent(window, "online");

        offline.subscribe(() => {
            const NoInternet = this.toastCtrl.create({
                message: 'יש לבדוק חיבור לאינטרנט',
                showCloseButton: true,
                closeButtonText: 'Ok'
            });
            NoInternet.present();
            //this.Toast.presentToast('יש לבדוק חיבור לאינטרנט');
        });

        online.subscribe(() => {
            console.log('Online event was detected.');
        })



        if (this.platform.is('cordova')) {
            if (this.platform.is('android')) {
              this.initializeFireBaseAndroid();
            }
            if (this.platform.is('ios')) {
                this.initializeFireBaseIos();
            }
        }



        console.log(localStorage.getItem("id"))


      if(localStorage.getItem("userid") != null && localStorage.getItem("userid") != '' )
          this.nav.setRoot('MainPage');
      else
          this.nav.setRoot('RegisterIntroPage');

    });
  }

    private initializeFireBaseAndroid(): Promise<any> {
        return this.firebase.getToken()
            .catch(error =>
                console.error('Error getting token', error)
            )
            .then(token => {
                this.firebase.subscribe('all').then((result) => {
                    if (result) console.log(`Subscribed to all`);
                    this.subscribeToPushNotificationEvents();
                });
            });
    }

    private initializeFireBaseIos(): Promise<any> {
        return this.firebase.grantPermission()
            .catch(error => console.error('Error getting permission', error))
            .then(() => {
                this.firebase.getToken()
                    .catch(error => console.error('Error getting token', error))
                    .then(token => {
                        console.log(`The token is ${token}`);
                        this.firebase.subscribe('all').then((result) => {
                            if (result) console.log(`Subscribed to all`);
                            this.subscribeToPushNotificationEvents();
                        });
                    });
            })

    }

    private saveToken(token: any): Promise<any> {
        // Send the token to the server
        // console.log('Sending token to the server...');
        return Promise.resolve(true);
    }

    private subscribeToPushNotificationEvents(): void {

        // Handle token refresh
        this.firebase.onTokenRefresh().subscribe(
            token => {

                this.auth.SetUserPush('SetUserPush',token,window.localStorage.userid);
                //this.saveToken(token);
                //this.loginService.sendToken('GetToken', token).then((data: any) => {
                //    console.log("UserDetails : ", data);
                //});
            },
            error => {
                console.error('Error refreshing token', error);
            });

        // Handle incoming notifications
        this.firebase.onNotificationOpen().subscribe(
            (notification) => {
                // !notification.tap
                //     ? alert('The user was using the app when the notification arrived...')
                //     : alert('The app was closed when the notification arrived...');
                if (!notification.tap) {
                    //alert(JSON.stringify(notification));
                }
                else {
                    //alert(JSON.stringify(notification));
                    if (notification.from_admin == "1")
                        this.nav.push(PushnotificationsPage);

                }
            },
            error => {
                console.error('Error getting the notification', error);
            });
    }



  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}
