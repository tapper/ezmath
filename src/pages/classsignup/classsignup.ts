import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {ServerService} from "../../services/server-service";
import {ToastService} from "../../services/toast-service";
import { ModalController } from 'ionic-angular';
import { ClasssignupmodalPage } from '../classsignupmodal/classsignupmodal';

import moment from 'moment';
import {ContactModalPage} from "../contact-modal/contact-modal";

/**
 * Generated class for the ClasssignupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-classsignup',
  templateUrl: 'classsignup.html',
})
export class ClasssignupPage implements OnInit {
    
    public todaydate :any  = moment().format('YYYY-MM-DD');
    public Classrow :any = [];

    public info:any = {
      'branchselect' : '',
      'professionselect' : '',
        'class_date' : this.todaydate,
        'class_end_date' : this.todaydate
  }

  public brancesArray:any;
    public professionArray:any;
    public searchArray:any = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, public server: ServerService,
              public Toast:ToastService,public modalCtrl: ModalController ) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ClasssignupPage');
  }

    async ngOnInit() {
        this.getBranches();
        this.GetProfessions();
    }

    getBranches() {
        this.server.GetBranches('GetBranches').then((data: any) => {
            console.log("GetBranches : " , data.json());
            this.brancesArray = data.json();
        });
    }

    GetProfessions() {
        this.server.GetBranches('GetProfessions').then((data: any) => {
            console.log("GetProfessions : " , data.json());
            this.professionArray = data.json();
        });
    }

    searchClass() {

      if (!this.info.professionselect)
            this.Toast.presentToast('יש לבחור מקצוע');


      else if (!this.info.branchselect)
          this.Toast.presentToast('יש לבחור סניף');


        else if (!this.info.class_date)
            this.Toast.presentToast('יש לבחור תאריך');

      else if (!this.info.class_end_date)
          this.Toast.presentToast('יש לבחור תאריך סיום');



        else {
            this.server.SearchClasses('SearchClasses',this.info,localStorage.getItem("userid")).then((data: any) => {
                console.log("SearchClasses : " , data.json());
                this.searchArray = data.json();

                if (data.json() == 0)
                    this.Toast.presentToast('לא נמצאו תוצאות');
            });
        }
    }

    registerUserToClass(row) {
      this.Classrow = row;

        let date:any = new Date()
        let hours:any = date.getHours()
        let minutes:any = date.getMinutes()
        let seconds:any = date.getSeconds()

        if (hours < 10)
            hours = "0" + hours

        if (minutes < 10)
            minutes = "0" + minutes
        let time:any = hours+':'+minutes;



        let dtToday:any = new Date();
        let month:any = dtToday.getMonth() + 1;
        let day:any = dtToday.getDate();
        let year:any = dtToday.getFullYear();
        if(month < 10)
            month = '0' + month.toString();
        if(day < 10)
            day = '0' + day.toString();

        let todayDate:any =   year + '-' + month + '-' + day;


        if (row.already_signup == 1)
          this.Toast.presentToast('הינך כבר רשום לשיעור זה');
        else {

            if (row.old_date < todayDate) {
                this.Toast.presentToast('לא ניתן להרשם לשיעור בתאריך קטן מהיום');
                return;
            } else {
                if (row.old_date == todayDate)
                {
                    if (time > row.class_hour  ) {
                        this.Toast.presentToast('לא ניתן להרשם לשיעור שעת הרשמה לשיעור זה הסתיימה');
                        return;
                    }
                }
            }

          this.server.registerUserToClass('registerUserToClass',localStorage.getItem("userid"),row.id).then((data: any) => {
              console.log("registerUserToClass : " , data.json());
              let response = data.json();
              if (response.max_quan_reached == 0) {
                  //row.already_signup = 1;
                  let myModal = this.modalCtrl.create(ClasssignupmodalPage,{details: row});
                  myModal.present();
              }

              else {
                  this.Toast.presentToast('לא ניתן להרשם לשיעור זה, מכסת תלמידים לשיעור הסתיים');
              }
          });
      }
    }

    OpenContactModal() {
        let ContactModal = this.modalCtrl.create(ContactModalPage);
        ContactModal.present();
    }
}
