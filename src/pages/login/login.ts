import { Component ,OnInit} from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AppSettings } from '../../services/app-settings';
import {FormControl, FormGroup, Validators,NgForm} from "@angular/forms";
import {AuthService} from "../../services/auth-service";
import {ToastService} from "../../services/toast-service";
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';


@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})

export class LoginPage implements OnInit {

    registerForm: FormGroup;
    isLoggedIn:boolean = false;
    userData: any;

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public auth:AuthService,
                public toast:ToastService,private fb: Facebook) {
    }


    async ngOnInit() {
    }


  onSubmit(form:NgForm)
  {
      console.log(form.value);
      this.auth.UserLogin('UserLogin',form.value,localStorage.getItem("push_id")).then((data: any) => {
        console.log("loginUser : " , data);
        let response = data.json()
        if (response.length > 0)
        {
            localStorage.setItem("userid", response[0].id);
            localStorage.setItem("userType", response[0].userType);
            this.navCtrl.setRoot('MainPage');
        }
        else
        {
            this.toast.presentToast("מייל או סיסמה שגוים יש לנסות שוב");
        }
      });
  }



  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

}
