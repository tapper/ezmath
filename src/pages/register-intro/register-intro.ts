import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';
import { AppSettings } from '../../services/app-settings'
import {AuthService} from "../../services/auth-service";
import {ToastService} from "../../services/toast-service";
/**
 * Generated class for the RegisterIntroPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-register-intro',
    templateUrl: 'register-intro.html',
})
export class RegisterIntroPage {

    isLoggedIn:boolean = false;
    userData: any;

    constructor(public navCtrl: NavController, public navParams: NavParams,private fb: Facebook,public auth:AuthService,public toast:ToastService) {
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad RegisterIntroPage');
        //this.fb.browserInit(AppSettings.FACEBOOKID, "v2.8");
    }

    goLogin()
    {
        this.navCtrl.push('LoginPage');
    }
    goToRegister(type)
    {
        this.navCtrl.push('RegisterPage', {type:type});
    }




}
