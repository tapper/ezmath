import {BrowserModule} from '@angular/platform-browser';
import {ErrorHandler, NgModule} from '@angular/core';
import {IonicApp, IonicErrorHandler, IonicModule} from 'ionic-angular';

import {MyApp} from './app.component';
import {AppSettings} from '../services/app-settings'

import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {AuthService} from "../services/auth-service";
import {ServerService} from "../services/server-service";
import {IonicStorageModule} from "@ionic/storage";
import {HttpModule} from "@angular/http";
import {ToastService} from "../services/toast-service";
import { Facebook } from '@ionic-native/facebook';
import {Firebase} from "@ionic-native/firebase";
import {ComponentsModule} from '../components/components.module';
import { ClasssignupmodalPage } from '../pages/classsignupmodal/classsignupmodal';
import { ContactModalPage } from '../pages/contact-modal/contact-modal';
import {PreviousticketsPage} from "../pages/previoustickets/previoustickets";

import {ClasssignupPage} from "../pages/classsignup/classsignup";
import {Network} from "@ionic-native/network";
import {PushnotificationsPage} from "../pages/pushnotifications/pushnotifications";
//import { ConnectivityServiceProvider } from '../providers/connectivity-service/connectivity-service';


@NgModule({
    declarations: [
        MyApp,
        ClasssignupmodalPage,
        ContactModalPage,
        ClasssignupPage,
        PushnotificationsPage,
        PreviousticketsPage,
    ],
    imports: [
        BrowserModule,
        IonicModule.forRoot(MyApp),
        IonicStorageModule.forRoot(),
        HttpModule,
        ComponentsModule,
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp,
        ClasssignupmodalPage,
        ContactModalPage,
        ClasssignupPage,
        PushnotificationsPage,
        PreviousticketsPage,
    ],
    providers: [
        StatusBar,
        SplashScreen,
        AuthService,
        ServerService,
        ToastService,
        Facebook,
        Firebase,
        {provide: ErrorHandler, useClass: IonicErrorHandler},
        Network,
        //ConnectivityServiceProvider
    ]
})
export class AppModule {
}
