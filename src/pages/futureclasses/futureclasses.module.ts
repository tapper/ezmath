import { NgModule } from '@angular/core';
import {IonicModule, IonicPageModule} from 'ionic-angular';
import { FutureclassesPage } from './futureclasses';
import {ComponentsModule} from "../../components/components.module";

@NgModule({
  declarations: [
    FutureclassesPage,
  ],
  imports: [
    IonicPageModule.forChild(FutureclassesPage),
     ComponentsModule,

  ],
})
export class FutureclassesPageModule {}
