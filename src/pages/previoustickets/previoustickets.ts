import { Component ,OnInit} from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {ServerService} from "../../services/server-service";

/**
 * Generated class for the PreviousticketsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-previoustickets',
  templateUrl: 'previoustickets.html',
})
export class PreviousticketsPage implements OnInit {

    public Type: any = 1;
    public userTicketsArray: any = [];
    public paymentsArray: any = [];
    public paymentMethod : any = ["מזומן","כרטיסיה","חוב"];

  constructor(public navCtrl: NavController, public navParams: NavParams,public server: ServerService) {
  }

    changeType(type) {
      this.Type = type;
    }


    async getTickets() {

        await this.server.GetpreviousTickets('GetpreviousTickets',localStorage.getItem("userid")).then((data: any) => {
            console.log("GetpreviousTickets : " , data.json());
            let response  = data.json();
            this.userTicketsArray = response.tickets;
            this.paymentsArray = response.payments;
        });

    }
    async ngOnInit() {
      this.getTickets();
    }


  ionViewDidLoad() {
    console.log('ionViewDidLoad PreviousticketsPage');
  }

}
